import re
import time

from concurrent.futures import ThreadPoolExecutor
from concurrent.futures import as_completed

p = re.compile("\\(?([0-9]{3})\\)?-([0-9]{3})-([0-9]{4})\s*$")

def parse_phone(val: str):
    if val is None:
        return {"phone": None, "valid": True}
    m = p.match(val)
    if not m:
        return {"phone": val, "valid": False}
    else:
        return {"phone": f"{m.group(1)}{m.group(2)}{m.group(3)}", "valid": True}

def parse_phone_delay(val: str, delay: float = 0.3):
    time.sleep(delay)
    return parse_phone(val)

if __name__ == '__main__':
    from sys import argv
    for v in argv[1:]:
        print(parse_phone_delay(v))
