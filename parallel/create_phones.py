import random

def get_random_phone():
    good = random.random() > 0.05
    part1 = random.randint(100, 999)
    part2 = random.randint(100, 999)
    part3 = random.randint(1000, 9999)
    if good:
        if random.random() < 0.5:
            return f"({part1})-{part2}-{part3}"
        else:
            return f"{part1}-{part2}-{part3}"
    else:
            return f"{part1}/{part2}/{part3}"

def create_file(filename: str, cnt: int):
    with open(filename, 'w') as f:
        for _ in range(cnt):
            f.write(f"{get_random_phone()}\n")

if __name__ == '__main__':
    from sys import argv
    if len(argv) != 3:
        exit(1)
    create_file(argv[1], int(argv[2]))
