## Python Optimization

![Python Performance Diagram](Python_Perf.png "Python Performance")

## Typical Choices
#### Must identify CPU bound / IO bound modules
#### CPU bound code will need multiple cores to effectively parallelize

1. Extensive numeric computations or string manipulations:
    1. Libraries like numpy, pandas
    1. Pararrel computing multi core
2. Complex data processing
    1. Better algorithms
    1. Better data structures
    1. Parallel computing multi core
3. Extensive calls to external systems (APIs, Databases etc.) (Network/IO with latency)
    1. Parallel computing single core or multi core


## For external calls with latency

1. We do not need many CPUs to makes may (100s-1000s) of simultaneous calls because CPU is not involved while a call is made
and the system is waiting for a response
2. Both AsyncIO and MultiThreading modules are appropriate even though both use a single core
3. AsyncIO is a bit more complicated and needs thorough testing
4. Multithreading is failrly simple to implement

```
from concurrent.futures import ThreadPoolExecutor
from concurrent.futures import as_completed

with ThreadPoolExecutor(max_workers = parallelism) as executor:
    results = executor.map(function_name, argument_list)

## Same as:
results = [function_name(arg) for arg in argument_list]

## If function takes more than one argument, use a wrapper function that uses a single tuple, expand it and call the
## original function
```

## Important Considerations

1. Know what type of optimization would work in your scenario
1. Have a baseliine to compare your results against
1. Before hitting an external site many times in parallel, consider the impact on that system
1. Check for bulk processing APIs instead of parallel calls, they would be more efficient on both your side and on the
   remore server
